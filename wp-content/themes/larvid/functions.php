<?php

add_action( 'admin_menu', 'remove_menus' );
function remove_menus() {
	remove_menu_page( 'index.php' );                  //Консоль
	remove_menu_page( 'edit.php' );                   //Записи
	remove_menu_page( 'upload.php' );                 //Медиафайлы
	//remove_menu_page( 'edit.php?post_type=page' );    //Страницы
	remove_menu_page( 'edit-comments.php' );          //Комментарии
	remove_menu_page( 'themes.php' );                 //Внешний вид
	remove_menu_page( 'plugins.php' );                //Плагины
	remove_menu_page( 'users.php' );                  //Пользователи
	remove_menu_page( 'tools.php' );                  //Инструменты
	//remove_menu_page( 'options-general.php' );        //Настройки
}

add_action( 'wp_enqueue_scripts', 'larkit_theme_scripts' );
function larkit_theme_scripts() {

	wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/js/theme.js', array( 'jquery' ), FALSE, TRUE );
	wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), FALSE, TRUE );
	wp_localize_script( 'custom-script', 'ajaxVars', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_ajax_do_vote', 'larkit_vote' );
add_action( 'wp_ajax_nopriv_do_vote', 'larkit_vote' );
function larkit_vote() {
	global $_POST, $_COOKIE;

	check_ajax_referer( 'larkit_video_vote', '_ajax_nonce' );

	$url = 'https://www.google.com/recaptcha/api/siteverify?secret=6LdNaRATAAAAABfIq80emBxLYMj2pMu1gAoOfZpo&response=' . $_POST['captcha_response'] . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
	$response = wp_remote_get( $url );

	if ( $response['success'] !== FALSE AND ! empty( $_POST['video_id'] ) ) {
		$video_id = intval( $_POST['video_id'] );
		if ( empty( $_COOKIE[ 'voted_' . $video_id ] ) ) {
			setcookie( 'voted_' . $video_id, 1, time() + 3600 * 24 * 7, "/" );
			$votes = larvid_like( $video_id );
			echo( json_encode( array( 'voted' => $votes ) ) );
		} else {
			echo( json_encode( array( 'voted' => - 1 ) ) );
		}
	}

	wp_die();
}

add_action( 'wp_ajax_do_unvote', 'larkit_unvote' );
add_action( 'wp_ajax_nopriv_do_unvote', 'larkit_unvote' );
function larkit_unvote() {
	global $_REQUEST, $_COOKIE;

	check_ajax_referer( 'larkit_video_vote', '_ajax_nonce' );

	$url = 'https://www.google.com/recaptcha/api/siteverify?secret=6LdNaRATAAAAABfIq80emBxLYMj2pMu1gAoOfZpo&response=' . $_POST['captcha_response'] . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
	$response = wp_remote_get( $url );

	if ( $response['success'] !== FALSE AND ! empty( $_POST['video_id'] ) ) {
		$video_id = intval( $_POST['video_id'] );
		if ( empty( $_COOKIE[ 'voted_' . $video_id ] ) ) {
			setcookie( 'voted_' . $video_id, 1, time() + 3600 * 24 * 7, "/" );
			$votes = larvid_unlike( $video_id );
			echo( json_encode( array( 'voted' => $votes ) ) );
		} else {
			echo( json_encode( array( 'voted' => - 1 ) ) );
		}
	}

	wp_die();
}
