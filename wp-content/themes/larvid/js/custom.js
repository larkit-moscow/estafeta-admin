/**
 * Datetime countdown start
 */

(function($){

	// Количество секунд в каждом временном отрезке
	var days = 24 * 60 * 60,
		hours = 60 * 60,
		minutes = 60;

	// Создаем плагин
	$.fn.countdown = function(prop){

		var options = $.extend({
			callback: function(){
			},
			timestamp: 0
		}, prop);

		var left, d, h, m, s;

		(function tick(){

			// Осталось времени
			left = Math.floor((options.timestamp - (new Date())) / 1000);

			if (left < 0) {
				left = 0;
			}

			// Осталось дней
			d = Math.floor(left / days);
			left -= d * days;

			// Осталось часов
			h = Math.floor(left / hours);
			left -= h * hours;

			// Осталось минут
			m = Math.floor(left / minutes);
			left -= m * minutes;

			// Осталось секунд
			s = left;

			// Вызываем возвратную функцию пользователя
			options.callback(d, h, m, s);

			// Планируем следующий вызов данной функции через 5 секунд
			setTimeout(tick, 5000);
		})();

		return this;
	};

	var dateSource = $('#timer-wrapper').data('date_source'),
		ts = new Date(dateSource * 1000);

	$('#countdown').countdown({
		timestamp: ts,
		callback: function(days, hours, minutes, seconds){

			if (hours < 10) {
				hours = '0' + hours;
			}
			if (minutes < 10) {
				minutes = '0' + minutes;
			}

			$('#days-left').html(days);
			$('#hours-left').html(hours);
			$('#minutes-left').html(minutes);

			var dayEndingArray = new Array('День', 'Дня', 'Дней'),
				hourEndingArray = new Array('Час', 'Часа', 'Часов'),
				minEndingArray = new Array('Минута', 'Минуты', 'Минут'),
				dayStr = getNumEnding(days, dayEndingArray),
				hourStr = getNumEnding(hours, hourEndingArray),
				minStr = getNumEnding(minutes, minEndingArray);

			$('#vote-days').text(dayStr);
			$('#vote-hours').text(hourStr);
			$('#vote-mins').text(minStr);

			// морфология
			function getNumEnding(number, endingArray){
				number = number % 100;
				var ending = [];
				if (number >= 11 && number <= 19) {
					ending = endingArray[2];
				} else {
					i = number % 10;
					switch (i) {
						case ( 1 ):
							ending = endingArray[0];
							break;
						case ( 2 ):
						case ( 3 ):
						case ( 4 ):
							ending = endingArray[1];
							break;
						default:
							ending = endingArray[2];
					}
				}

				return ending;
			}


		}
	});

})(jQuery);

/* datetime countdown end */

var scrolling = false;
jQuery(document).ready(function($){
	// активные пункты меню
	$('#cd-top-nav ul a').click(function(){
		scrolling = true;
		$('#cd-top-nav ul a').removeClass('active-menu');
		$(this).addClass('active-menu');
		setTimeout(function(){
			scrolling = false;
		}, 2000);
	});

	// нравится
	$('.btn-like').click(function(e){
		e.preventDefault();

		var $btn = $(this),
			$parent = $btn.parent(),
			$column = $btn.closest('.col-inner'),
			captchaID = $column.find('.grecaptcha').attr('id');

		var verifyCallbackRecaptcha = function(response){
			var vars = {
				action: 'do_vote',
				_ajax_nonce: $btn.attr('data-nonce'),
				video_id: $(this).parent().find('span').html(),
				captcha_response: response
			};

			$.post(ajaxVars.ajaxurl, vars, function(response){
				var data = $.parseJSON(response);
				if (data.voted == -1) {
					alert('Вы уже голосовали за это видео!');
				}
				else {
					var votes = data.voted.toString().split('');
					var vote_txt = '<span class="bg-main text-white">' + votes.join('</span>&nbsp;<span class="bg-main text-white">') + '</span>';
					$('.votes-4-' + vars.video_id).html(vote_txt);
					$parent.find('.btn-like, .btn-unlike').remove();
					$parent.html('<p class="no-vote">Спасибо! Ваш голос учтен.</p>');
				}
			});
		}.bind($btn);

		grecaptcha.render(captchaID, {
			'sitekey': '6LdNaRATAAAAANnJV66wlTARL4moie1Wv6lBEUXB',
			'callback': verifyCallbackRecaptcha
		});


	});


	// не нравится
	$('.btn-unlike').click(function(e){
		e.preventDefault();

		var $btn = $(this),
			$parent = $btn.parent(),
			$column = $btn.closest('.col-inner'),
			captchaID = $column.find('.grecaptcha').attr('id');

		var verifyCallbackRecaptcha = function(response){
			var vars = {
				action: 'do_unvote',
				_ajax_nonce: $btn.attr('data-nonce'),
				video_id: $btn.parent().find('span').html(),
				captcha_response: response
			};

			$.post(ajaxVars.ajaxurl, vars, function(response){
				var data = $.parseJSON(response);
				if (data.voted == -1) {
					alert('Вы уже голосовали за это видео!');
				}
				else {
					var votes = data.voted.toString().split('');
					var vote_txt = '<span class="bg-main text-white">' + votes.join('</span>&nbsp;<span class="bg-main text-white">') + '</span>';
					$('.votes-4-' + vars.video_id).html(vote_txt);
					$parent.find('.btn-like, .btn-unlike').remove();
					$parent.html('<p class="no-vote">Спасибо! Ваш голос учтен.</p>');
				}
			});
		}.bind($btn);

		grecaptcha.render(captchaID, {
			'sitekey': '6LdNaRATAAAAANnJV66wlTARL4moie1Wv6lBEUXB',
			'callback': verifyCallbackRecaptcha
		});

	});

	$(document).scroll(function(){
		if (!scrolling) {
			$('#cd-top-nav ul a').removeClass('active-menu');
		}
	});

	// список участников
	$('#show-list').click(function(e){
		var body = document.body,
			html = document.documentElement;
		var height = Math.max(body.scrollHeight, body.offsetHeight,
			html.clientHeight, html.scrollHeight, html.offsetHeight);
		$('#admit-list').css('height', height + 'px');
		$('#admit-list').css('padding-top', ($(document).scrollTop() - 20) + 'px');
		$('#admit-list').show();
		e.preventDefault();
	});

	$('.close-list').click(function(e){
		$('#admit-list').hide();
		e.preventDefault();
	});

	// архив
	$('.show-more-archieve').click(function(e){
		var btn_del = $('.show-more-wrap'),
			from = $(this).attr('href').replace(/#/g, '');

		$.get('/?page_id=47&from=' + from, function(data){
			btn_del.after(data);
			btn_del.remove();
		});
		e.preventDefault();
	});

})
;
