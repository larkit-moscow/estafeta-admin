<?php
get_header();
$ajax_nonce = wp_create_nonce( 'larkit_video_vote' );
?>

<!-- Page preloader (display loading animation while page loads) -->
<div id="preloader">
	<div class="pulse"></div>
	<p class="pre-preloader">Идет загрузка...</p>
</div>

<!-- Begin page borders -->
<div class="border-top"></div>
<div class="border-bottom"></div>
<div class="border-left"></div>
<div class="border-right"></div>
<!-- End page borders -->


<!-- /////////////////////////////
////////// Begin header //////////
////////////////////////////// -->

<header class="is-fixed">
	<a id="cd-logo" href="#"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo.jpg" alt="image"></a>
	<nav id="cd-top-nav" class="navbar hidden-xs hidden-sm">
		<ul>
			<li><a href="#section-1" class="page-scroll">Голосование</a></li>
			<li><a href="#section-5" class="page-scroll">Участники</a></li>
			<li><a href="#section-6" class="page-scroll">О проекте</a></li>
			<li><a href="#section-9" class="page-scroll">Расписание</a></li>
			<!-- <li><a href="#section-7" class="page-scroll">Помощь</a></li>
			<li><a href="#section-9" class="page-scroll">Организаторы</a></li> -->
		</ul>
	</nav>

	<nav id="cd-top-mobile-nav" class="visible-xs visible-sm hidden-md hidden-lg">
		<ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Меню
					<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#section-1" class="page-scroll">Голосование</a></li>
					<li><a href="#section-5" class="page-scroll">Участники</a></li>
					<li><a href="#section-6" class="page-scroll">О проекте</a></li>
					<li><a href="#section-9" class="page-scroll">Расписание</a></li>
					<!-- <li><a href="#section-7" class="page-scroll">Помощь</a></li>
					<li><a href="#section-9" class="page-scroll">Организаторы</a></li> -->
				</ul>
			</li>
		</ul>
	</nav>
	<!-- Menu trigger (menu button) -->
	<!--a id="cd-menu-trigger" href="#0"><span class="cd-menu-icon"></span></a-->
</header>

<!-- /////////////////////////////
////////// End header //////////
////////////////////////////// -->


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////
////////// Begin menu  (more info: http://codyhouse.co/gem/secondary-expandable-navigation/) //////////
/////////////////////////////////////////////////////////////////////////////////////////////////// -->


<!-- /////////////////////////
////////// End menu //////////
////////////////////////// -->


<!-- ///////////////////////////////////
////////// Begin body content //////////
//////////////////////////////////// -->

<div id="body-content">


	<!-- Begin intro section (Parallax) -->
	<section id="section-intro" class="intro-parallax full-height">

		<!-- Element background image (parallax) -->
		<div class="full-cover bg-image" data-stellar-ratio="0.2" style="background-image: url(<?php bloginfo( 'template_url' ); ?>/img/happy-kinder.jpg);"></div>

		<!-- Element cover -->
		<div class="cover"></div>

		<!-- Intro caption -->
		<div class="intro-caption text-white">
			<h1><span class="bg-main text-white">Наши</span> общие<br>
				возможности – <br>
				наши общие результаты.
			</h1>

			<p>Эстафета фестивалей межрайонных советов директоров школ</p>
		</div>

		<!-- Made with love :) -->
		<!--div class="made-with-love hidden-xs" data-stellar-ratio="0.2">
			<p class="text-white">made with <span class="text-red"><i class="fa fa-heart-o"></i></span></p>
		</div-->

		<!-- Scroll down arrow -->
		<a class="scroll-down-arrow page-scroll text-center" href="#section-1"><span class="text-white"><i class="fa fa-arrow-down"></i></span></a>

	</section>
	<!-- End intro section (Parallax) -->


	<!-- Section 1 -->
	<section id="section-1" class="welcome bg-blue text-white">
		<div class="container">
			<div class="row">

				<div class="col-left col-md-6">
					<div class="col-inner">

						<!-- Begin Heading -->
						<div class="heading heading-xlg">
							<h1><span class="bg-white text-blue">Голосование</span></h1>
						</div>
						<!-- End Heading -->

						<p class="lead">За лучший ролик эстафеты</p>

					</div>
					<!-- /.col-inner -->
				</div>
				<!-- /.col -->

				<div class="col-right col-md-6">
					<div class="col-inner">

						<p class="lead">
							Фестивали будут проводиться по субботам в течение 2015/2016 учебного года на площадках
							школ Москвы. В дни Фестиваля все школы межрайонного совета будут представлять свои учебные
							и внеучебные достижения за последние 5 лет, демонстрировать образовательные
							возможности, знакомить с дополнительными услугами.  Смотрите видеорепортажи о фестивале и
							голосуйте за понравившиеся видеоролики!
						</p>

					</div>
				</div>
				<!-- /.col -->

			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</section>

	<!-- Section 5 -->
	<?php
	// xdebug_break();
	$videos = larvid_get_active_videos();
	if ( ! empty( $videos ) && is_array( $videos ) && count( $videos ) >= 1 ):
		?>
		<section id="section-2" class="video-demonstration split-box no-padding">
			<div class="container-fluid">
				<div class="row">

					<!-- Begin responsive columns of same height (more info: http://www.minimit.com/articles/solutions-tutorials/bootstrap-3-responsive-columns-of-same-height) -->
					<div class="row-same-height">

						<div class="col-left col-lg-6 col-lg-height no-padding">
							<div class="col-inner no-padding">

								<!-- Begin embed video (more info about reffering embed videos: https://www.feedthebot.com/pagespeed/defer-videos.htm) -->
								<div class="embed-responsive embed-responsive-16by9">
									<video src="<?=$videos[0]->link?>" width="500" height="281" poster="<?=$videos[0]->preview?>" controls/>
									<!--iframe data-src="https://player.vimeo.com/video/60616160?" width="500" height="281" allowfullscreen></iframe-->
								</div>
								<!-- End embed video -->

							</div>
							<!-- /.col-inner -->
						</div>
						<!-- /.col -->

						<div class="col-right col-lg-6 col-lg-height col-middle no-padding bg-light-gray">
							<div class="col-inner">

								<!-- Begin Heading -->
								<div class="heading heading-xlg">
									<h1>Видео <span class="bg-main text-white">#<?=$videos[0]->number?></span></h1>
								</div>
								<!-- End Heading -->

								<p class="lead"><?=$videos[0]->description?></p>
								<?php
								$votes = str_split( $videos[0]->votes );
								$votes = '<span class="bg-main text-white">' . implode( '</span>&nbsp;<span class="bg-main text-white">', $votes ) . '</span>';
								?>
								<p class="lead voted"><span class="text-dark">Проголосовало</span>
									<span class="votes-4-<?=$videos[0]->id?>"><?=$votes?></span></p>

								<p>
									<span class="noshow"><?=$videos[0]->id?></span><a href="#" data-nonce="<?php echo $ajax_nonce; ?>" class="btn btn-blue btn-rounded btn-lg btn-unlike">Не
										нравится</a>&nbsp;&nbsp;&nbsp;<a href="#" data-nonce="<?php echo $ajax_nonce; ?>" class="btn btn-blue btn-rounded btn-lg btn-like">Нравится</a>
								</p>
								<div class="grecaptcha" id="html_element_<?=$videos[0]->id?>"></div>
							</div>
							<!-- /.col-inner -->
						</div>
						<!-- /.col -->

					</div>
					<!-- End responsive columns of same height -->

				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
		<?php if ( count( $videos ) >= 2 ): ?>
		<!-- Section 3 -->
		<section id="section-3" class="video-demonstration split-box no-padding hidden-xs hidden-sm hidden-md visible-lg">
			<div class="container-fluid">
				<div class="row">

					<!-- Begin responsive columns of same height (more info: http://www.minimit.com/articles/solutions-tutorials/bootstrap-3-responsive-columns-of-same-height) -->
					<div class="row-same-height">

						<div class="col-left col-lg-6 col-lg-height col-middle no-padding bg-light-gray">
							<div class="col-inner">

								<!-- Begin Heading -->
								<div class="heading heading-xlg">
									<h1>Видео <span class="bg-main text-white">#<?=$videos[1]->number?></span></h1>
								</div>
								<!-- End Heading -->

								<p class="lead"><?=$videos[1]->description?></p>
								<?php
								$votes = str_split( $videos[1]->votes );
								$votes = '<span class="bg-main text-white">' . implode( '</span>&nbsp;<span class="bg-main text-white">', $votes ) . '</span>';
								?>
								<p class="lead voted"><span class="text-dark">Проголосовало</span>
									<span class="votes-4-<?=$videos[1]->id?>"><?=$votes?></span></p>

								<p>
									<span class="noshow"><?=$videos[1]->id?></span><a href="#" data-nonce="<?php echo $ajax_nonce; ?>" class="btn btn-blue btn-rounded btn-lg btn-unlike">Не
										нравится</a>&nbsp;&nbsp;&nbsp;<a href="#" data-nonce="<?php echo $ajax_nonce; ?>" class="btn btn-blue btn-rounded btn-lg btn-like">Нравится</a>
								</p>
								<div class="grecaptcha" id="html_element_<?=$videos[1]->id?>"></div>
							</div>
							<!-- /.col-inner -->
						</div>
						<!-- /.col -->

						<div class="col-right col-lg-6 col-lg-height no-padding">
							<div class="col-inner no-padding">

								<!-- Begin embed video (more info about reffering embed videos: https://www.feedthebot.com/pagespeed/defer-videos.htm) -->
								<div class="embed-responsive embed-responsive-16by9">
									<!--iframe data-src="https://player.vimeo.com/video/60616160?" width="500" height="281" allowfullscreen></iframe-->
									<video src="<?=$videos[1]->link?>" width="500" height="281" poster="<?=$videos[1]->preview?>" controls/>
								</div>
								<!-- End embed video -->

							</div>
							<!-- /.col-inner -->
						</div>
						<!-- /.col -->


					</div>
					<!-- End responsive columns of same height -->

				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>

		<!-- Section 3.5 -->
		<section id="section-3_5" class="video-demonstration split-box no-padding visible-xs visible-sm visible-md hidden-lg">
			<div class="container-fluid">
				<div class="row">
					<!-- Begin responsive columns of same height (more info: http://www.minimit.com/articles/solutions-tutorials/bootstrap-3-responsive-columns-of-same-height) -->
					<div class="row-same-height">

						<div class="col-right col-lg-6 col-lg-height no-padding">
							<div class="col-inner no-padding">

								<!-- Begin embed video (more info about reffering embed videos: https://www.feedthebot.com/pagespeed/defer-videos.htm) -->
								<div class="embed-responsive embed-responsive-16by9">
									<!--iframe data-src="https://player.vimeo.com/video/60616160?" width="500" height="281" allowfullscreen></iframe-->
									<video src="<?=$videos[1]->link?>" width="500" height="281" poster="<?=$videos[1]->preview?>" controls/>
								</div>
								<!-- End embed video -->

							</div>
							<!-- /.col-inner -->
						</div>
						<!-- /.col -->

						<div class="col-left col-lg-6 col-lg-height col-middle no-padding bg-light-gray">
							<div class="col-inner">

								<!-- Begin Heading -->
								<div class="heading heading-xlg">
									<h1>Видео <span class="bg-main text-white">#<?=$videos[1]->number?></span></h1>
								</div>
								<!-- End Heading -->

								<p class="lead"><?=$videos[1]->description?></p>
								<?php
								$votes = str_split( $videos[1]->votes );
								$votes = '<span class="bg-main text-white">' . implode( '</span>&nbsp;<span class="bg-main text-white">', $votes ) . '</span>';
								?>
								<p class="lead voted"><span class="text-dark">Проголосовало</span>
									<span class="votes-4-<?=$videos[1]->id?>"><?=$votes?></span></p>

								<p><span class="noshow"><?=$videos[1]->id?></span></span>
									<a href="#" data-nonce="<?php echo $ajax_nonce; ?>" class="btn btn-blue btn-rounded btn-lg btn-unlike">Не
										нравится</a>&nbsp;&nbsp;&nbsp;<a href="#" data-nonce="<?php echo $ajax_nonce; ?>" class="btn btn-blue btn-rounded btn-lg btn-like">Нравится</a>
								</p>
								<div class="grecaptcha" id="html_element_m_<?=$videos[1]->id?>"></div>
							</div>
							<!-- /.col-inner -->
						</div>
						<!-- /.col -->

					</div>
					<!-- End responsive columns of same height -->

				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
	<?php endif; ?>
	<?php endif; ?>

	<?php
	// count vote date
	global $wpdb;
	$videos = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(id) FROM " . LARVID_TABLE_NAME . " WHERE status = %s;", 1 ) );

	$vote_date = get_option( 'vote_date' );
	$current_timestamp = time();
	if ( $vote_date AND $vote_date >= $current_timestamp AND $videos > 0 ) :

		?>
		<section id="section - 4" class="work split-box bg-blue">
			<div class="container timer" id="timer-wrapper" data-date_source=" <?php echo $vote_date; ?>">
				<p class="text-white">До окончания голосования осталось:</p>

				<div class="row" style="margin: 0 auto;">
					<!-- Count-1 -->
					<div class="col col-md-2 col-md-100 col-sm-6">
						<div class="heading-xlg">
							<h1 class="bg-main text-white" id="days-left">06</h1>

							<h3 id="vote-days">Дней</h3>
						</div>
					</div>
					<!-- /.col -->

					<!-- Count-1 -->
					<div class="col col-md-2 col-md-100 col-sm-6">
						<div class="heading-xlg">
							<h1 class="bg-main text-white" id="hours-left">15</h1>

							<h3 id="vote-hours">Часов</h3>
						</div>
					</div>
					<!-- /.col -->

					<!-- Count-1 -->
					<div class="col col-md-2 col-md-100 col-sm-6">
						<div class="heading-xlg">
							<h1 class="bg-main text-white" id="minutes-left">05</h1>

							<h3 id="vote-mins">Минут</h3>
						</div>
					</div>
					<!-- /.col -->
				</div>
			</div>

		</section>
	<?php endif; ?>

	<!-- Section 5 -->
	<section id="section-5" class="counter-up">
		<div class="container">
			<div class="row" style="margin-top: 40px;">
				<div class="col col-lg-12">
					<div class="col-inner">

						<!-- Begin Heading -->
						<div class="heading heading-xlg heading-center">
							<h1>Участники</h1>
						</div>

					</div>
				</div>
			</div>
			<div class="row">

				<!-- Count-1 -->
				<div class="col count-1 col-md-3 col-sm-6">
					<div class="counter-up-inner">
						<div class="counter-icon"><i class="fa fa-list-alt"></i></div>
						<div class="counter">19</div>
						<div class="divider divider-center"></div>
						<h4>Эстафет</h4>
					</div>
				</div>
				<!-- /.col -->

				<!-- Count-2 -->
				<div class="col count-2 col-md-3 col-sm-6">
					<div class="counter-up-inner">
						<div class="counter-icon"><i class="fa fa-users"></i></div>
						<div class="counter">37</div>
						<div class="divider divider-center"></div>
						<h4>Участников</h4>
					</div>
				</div>
				<!-- /.col -->

				<!-- Count-3 -->
				<div class="col count-3 col-md-3 col-sm-6">
					<div class="counter-up-inner">
						<div class="counter-icon"><i class="fa fa-trophy"></i></div>
						<div class="counter">5</div>
						<div class="divider divider-center"></div>
						<h4>Призовых мест</h4>
					</div>
				</div>
				<!-- /.col -->

				<!-- Count-4 -->
				<div class="col count-4 col-md-3 col-sm-6">
					<div class="counter-up-inner">
						<div class="counter-icon"><i class="fa fa-map-marker"></i></div>
						<div class="counter">146</div>
						<div class="divider divider-center"></div>
						<h4>Районов Москвы</h4>
					</div>
				</div>
				<!-- /.col -->

			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</section>
	<?php
	$weeks = larvid_get_archieved_videos();
	$last_number = 0;
	/*
	if ( ! empty( $weeks ) && is_array( $weeks ) ) {
		$i = 0;
		foreach ( $weeks as $dt => $w ) {
			$dt_prev = $dt - 7 * 24 * 60 * 60;
			$video_numbers = array();
			foreach ( $w as $v ) {
				$video_numbers[] = '#' . $v->number;
			}

			include( 'archieve.php' );

			if ( ++ $i >= 3 ) {
				break;
			}
		}
	}
	*/
	if ( is_array( $weeks ) && count( $weeks ) > 0 ):
		?>
		<section class="show-more-wrap">
			<div class="container">
				<div class="row">
					<div class="col col-lg-12">
						<div class="col-inner">
							<!-- Begin Heading -->
							<div class="heading-center">
								<a href="#<?php if ( ! empty( $last_number ) ) {
									echo $last_number;
								} ?>" class="btn btn-warning btn-rounded btn-lg show-more-archieve">Архив</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>

	<!-- Section 6 -->
	<section id="section-6" class="work split-box bg-image" style="background-image: url(<?php bloginfo( 'template_url' ); ?>/img/pattern-triangles.jpg);">

		<!-- element cover -->
		<div class="cover"></div>

		<div class="container-fluid text-white">
			<div class="row">

				<!-- Begin responsive columns of same height (more info: http://www.minimit.com/articles/solutions-tutorials/bootstrap-3-responsive-columns-of-same-height) -->
				<div class="row-same-height">

					<div class="col-left col-lg-6 col-lg-height col-middle no-padding">
						<div class="col-inner">

							<!-- Begin Heading -->
							<div class="heading heading-xlg">
								<h1>
									<span class="bg-white text-dark">О</span><span class="bg-main text-white">Проекте</span>
								</h1>
							</div>
							<!-- End Heading -->

							<p class="lead">В Москве стартовала эстафета Фестивалей межрайонных советов директоров школ
								«Наши общие возможности – наши общие результаты». Фестивали будут проводиться по
								субботам в течение 2015/2016 учебного года на площадках школ Москвы для демонстрации
								возможностей и результатов всех образовательных организаций, входящих в состав
								межрайонных советов. В дни Фестиваля все школы межрайонного совета будут представлять
								свои учебные и внеучебные достижения за последние 5 лет, демонстрировать образовательные
								возможности, знакомить с дополнительными услугами. К участию в Фестивалях приглашаются
								все жители районов: ученики и их родители, бабушки и дедушки, педагоги, представители
								органов власти, вузов, учреждений культуры, физической культуры и спорта, а также все
								желающие. Смотрите видеорепортажи о фестивале и голосуйте за понравившиеся
								видеоролики!</p>

							<br>
							<!-- <a href="#" class="btn btn-white btn-rounded btn-lg" id="show-list">Список участников</a> -->

						</div>
						<!-- /.col-inner -->
					</div>
					<!-- /.col -->

					<div class="col-right col-lg-6 col-lg-height">
						<div class="col-inner">

							<!-- Begin info box -->
							<div class="info-box-wrapper">
								<div class="row">

									<!-- info box 1 -->
									<div class="col col-sm-6 col-lg-6">
										<div class="info-box">
											<span class="info-box-icon"><i class="fa fa-bullseye"></i></span>

											<div class="info-box-heading">
												<h3>Цель</h3>

												<div class="divider"></div>
											</div>
											<div class="info-box-info">
												<p>Демонстрация возможностей и результатов всех образовательных
													организаций, входящих в МРСД за последние 5 лет.</p>
											</div>
										</div>
									</div>

									<!-- info box 2 -->
									<div class="col col-sm-6 col-lg-6">
										<div class="info-box">
											<span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>

											<div class="info-box-heading">
												<h3>Задачи</h3>

												<div class="divider"></div>
											</div>
											<div class="info-box-info">
												<p>Создание единой социокультурной среды; координация содержания работы
													в управлении образованием; консолидация родительского сообщества;
													разработка современных форм предъявления результатов школ
													москвичам.</p>
											</div>
										</div>
									</div>

									<!-- info box 3 -->
									<div class="col col-sm-6 col-lg-6">
										<div class="info-box">
											<span class="info-box-icon"><i class="fa fa-line-chart"></i></span>

											<div class="info-box-heading">
												<h3>Голосование</h3>

												<div class="divider"></div>
											</div>
											<div class="info-box-info">
												<p>Голосование за видеорепортаж возможно в течение одной недели после
													его размещения на сайте.</p>
											</div>
										</div>
									</div>

									<!-- info box 4 -->
									<div class="col col-sm-6 col-lg-6">
										<div class="info-box">
											<span class="info-box-icon"><i class="fa fa-trophy"></i></span>

											<div class="info-box-heading">
												<h3>Призы</h3>

												<div class="divider"></div>
											</div>
											<div class="info-box-info">
												<p>Победитель и призеры эстафеты фестивалей получат гранты на развитие
													новых технологий предъявления результатов школы москвичам.</p>
											</div>
										</div>
									</div>

									<!-- info box 3 -->
									<div class="col col-sm-6 col-lg-6">
										<div class="info-box">
											<span class="info-box-icon"><i class="fa fa-users"></i></span>

											<div class="info-box-heading">
												<h3>Участники</h3>

												<div class="divider"></div>
											</div>
											<div class="info-box-info">
												<p>Ученики и их родители, бабушки и дедушки, педагоги, представители
													органов власти, вузов, учреждений культуры, физической культуры и
													спорта, а также все желающие.</p>
											</div>
										</div>
									</div>

									<!-- info box 4 -->
									<div class="col col-sm-6 col-lg-6">
										<div class="info-box">
											<span class="info-box-icon"><i class="fa fa-university"></i></span>

											<div class="info-box-heading">
												<h3>Возможности</h3>

												<div class="divider"></div>
											</div>
											<div class="info-box-info">
												<p>Все школы межрайонного совета будут представлять свои учебные и
													внеучебные достижения за последние 5 лет, демонстрировать
													образовательные возможности, знакомить с дополнительными
													услугами.</p>
											</div>
										</div>
									</div>

								</div>
								<!-- /.row -->
							</div>
							<!-- End info box -->

						</div>
						<!-- /.col-inner -->
					</div>
					<!-- /.col -->
				</div>
				<!-- End responsive columns of same height -->

			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</section>


	<!-- Section 9 - Расписание -->
	<section id="section-9" class="blog no-padding">

		<div class="container-fluid">
			<div class="row" style="margin-top: 40px;">
				<div class="col col-lg-12">
					<div class="col-inner">

						<!-- Begin Heading -->
						<div class="heading heading-xlg heading-center">
							<h1>Расписание</h1>

							<p>даты проведения эстафеты Фестивалей</p>
						</div>

					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<table class="table table-responsive table-striped">
						<tr>
							<td>5.09.2015</td>
							<td><a href="http://lyc1557zg.mskobr.ru/">#1 Лицей №1557</a></td>
						</tr>
						<tr>
							<td>12.09.2015</td>
							<td><span class=""><a href="http://sch1228.mskobr.ru/">#2 Школа № 1228 с углубленным
										изучением английского
										языка</a></span><br/><span><a href="http://sch1400z.mskobr.ru/">#3 Школа №
										1400</a></span></td>
						</tr>
						<tr>
							<td>19.09.2015</td>
							<td><span class=""><a href="http://sch1494sv.mskobr.ru/">#4 Школа
										№1494</a> <br/> <a href="http://sch2065tn.mskobr.ru/">#5 Школа №2065</a>
						</tr>
						<tr>
							<td>26.09.2015</td>
							<td><span class=""><a href="http://gym1637.mskobr.ru/">#6 Гимназия
										№1637</a></span><br/><span><a href="http://sch2098s.mskobr.ru/">#7 Школа №2098
										им. Героя Советского Союза Л.М. Доватора</a></span></td>
						</tr>
						<tr>
							<td>03.10.2015</td>
							<td><span class=""><a href="http://sch1298sz.mskobr.ru/">#8 Школа
										№1298</a></span><br/><span><a href="http://sch2017u.mskobr.ru/">#9 Школа
										№2017</a></span></td>
						</tr>
						<tr>
							<td>17.10.2015</td>
							<td><a href="http://kurchat.mskobr.ru/">#10 Курчатовская школа</a></span>
								<br/><span><a href="http://gym1409s-new.mskobr.ru/">#11 Гимназия №1409</a></span></td>
						</tr>
						<tr>
							<td>24.10.2015</td>
							<td><a href="http://sch1259.mskobr.ru/">#12 Школа № 1259 с углубленным изучением иностранных
									языков</a></span><br/><span><a href="http://gym1554.mskobr.ru/">#13 Гимназия №
										1554</a></span></td>
						</tr>
						<tr>
							<td>31.10.2015</td>
							<td><a href="http://sch2036v.mskobr.ru/ads_edu/festival_mezhrajonnogo_soveta_direktorov_rajonov_veshnyaki_novokosino_i_kosino-uhtomskij/
 ">#14 Школа №2036</a></span>
								<br/><span> <a href="http://sch534.mskobr.ru/">#15 Школа №534</a></span></td>
						</tr>
						<tr>
							<td>14.11.2015</td>
							<td><a href="http://sch1357uv.mskobr.ru/">#16 Школа №1357 с углубленным изучением
									английского языка</a></span><br/><span> <a href="http://sch1995uz.mskobr.ru/ads_edu/uvazhaemye_zhiteli_moskvy_i_gosti_stolicy/">#17
										Школа №1995</a></span></td>
						</tr>
						<tr>
							<td>21.11.2015</td>
							<td><a href="http://gum1573.mskobr.ru/">#18 Гимназия №1573</a></span>
								<br/><span><a href="http://sch1002.mskobr.ru/">#19 Школа №1002</a></span></td>
						</tr>
						<tr>
							<td>28.11.2015</td>
							<td><a href="http://sch2089uv.mskobr.ru/mezhrajonnyj_sovet_direktorov_oo_rajonov_nekrasovka_-_zhulebino/mezhrajonnye_meropriyatiya/">#20 Школа №2089</a></span>
								<br/><span><a href="http://sch97z.mskobr.ru/novosti/my_priglashaem_vas_na_festival_nashi_obwie_vozmozhnosti_-_nashi_obwie_rezul_taty/">#21 Школа №97</a></span></td>
						</tr>
						<tr>
							<td>05.12.2015</td>
							<td><a href="http://gym1508.mskobr.ru/mezhrajonnyj_sovet/festival_mezhrajonnyh_sovetov_direktorov_shkol_nashi_obwie_vozmozhnosti_-_nashi_obwie_rezul_taty/">#22 Измайловская гимназия №1508</a></span>
								<br/><span><a href="http://sch924.mskobr.ru/mezhrajonnyj_sovet_direktorov/">#23 Школа №924</a></span></td>
						</tr>
						<tr>
							<td>12.12.2015</td>
							<td><a href="http://sch1352v.mskobr.ru/">#24 Школа №1352 с углубленным изучением английского
									языка</a></span><br/><span><a href="http://sch1474s.mskobr.ru/">#25 Школа
										№1474</a></span>
							</td>
						</tr>
						<tr>
							<td>16.01.2016</td>
							<td><a href="http://sch1394uv.mskobr.ru/">#26 Школа №1394 с углубленным изучением отдельных
									предметов</a></span><br/><span><a href="http://sch1363uv.mskobr.ru/">#27 Школа №1363
										с углубленным изучением отдельных предметов</a></span></td>
						</tr>
						<tr>
							<td>23.01.2016</td>
							<td><a href="http://2123.mskobr.ru/">#28 Школа №2123 им. Мигеля Эрнандеса</a></span>
								<br/><span><a href="http://sch2009uz.mskobr.ru/">#29 Школа №2009</a></span></td>
						</tr>
						<tr>
							<td>06.02.2016</td>
							<td><a href="http://gym1538sz.mskobr.ru/">#30 Гимназия №1538</a></span>
								<br/><span><a href="http://sch630.mskobr.ru/">#31 Школа №630 «Лингвистический
										центр»</a></span>
							</td>
						</tr>
						<tr>
							<td>13.02.2016</td>
							<td><a href="http://gym1599uv.mskobr.ru/">#32 Лицейско-гимназический комплекс на
									Юго-Востоке</a></span><br/><span><a href="http://sch1206uz.mskobr.ru/">#33 Школа
										№1206 с углубленным изучением английского языка</a></span></td>
						</tr>
						<tr>
							<td>19.03.2016</td>
							<td><a href="http://sch2054.mskobr.ru/">#34 Школа №2054</a></span>
								<br/><span><a href="http://gym1532uz.mskobr.ru/">#35 Гимназия №1532</a></span></td>
						</tr>
						<tr>
							<td>16.04.2016</td>
							<td><a href="http://sch7uz.mskobr.ru/">#36 Школа №7 с углубленным изучением математики и
									информатики</a></span><br/><span><a href="http://sch1329.mskobr.ru/">#37 Школа №
										1329</a></span></td>
						</tr>
					</table>
				</div>
			</div>

		</div>
		<!-- /.container -->
	</section>


	<!-- Section 7 -->
	<?php get_footer(); ?>
