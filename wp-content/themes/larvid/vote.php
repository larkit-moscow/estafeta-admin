<?php /* Template Name: Vote */
if(!empty($_REQUEST['id'])) {
    $video_id = intval($_REQUEST['id']);
    if(empty($_COOKIE['voted_'.$video_id])) {
        setcookie('voted_'.$video_id, 1, time()+3600*24*7, "/");
        $votes = larvid_like($video_id);
        die(json_encode(array('voted' => $votes)));
    }
    else {
        die(json_encode(array('voted' => -1)));
    }
}
?>