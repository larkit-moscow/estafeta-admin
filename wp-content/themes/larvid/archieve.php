<section class="archieve bg-blue">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12">
                <div class="col-inner">
                    <!-- Begin Heading -->
                    <div class="heading heading-center">
                        <h2>
                            <span class="bg-white text-dark"><?php echo date('d.m', $dt_prev); ?> - <?php echo date('d.m', $dt); ?></span>&nbsp;&nbsp;
                            <span class="bg-white text-dark"><?php echo implode(' и ', $video_numbers); ?></span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$odd = true;
$last_number = 0;
foreach($w as $v) :
    if($odd) :
        ?>
        <section class="video-demonstration split-box no-padding">
            <div class="container-fluid">
                <div class="row">

                    <!-- Begin responsive columns of same height (more info: http://www.minimit.com/articles/solutions-tutorials/bootstrap-3-responsive-columns-of-same-height) -->
                    <div class="row-same-height">

                        <div class="col-left col-lg-6 col-lg-height no-padding">
                            <div class="col-inner no-padding">

                                <!-- Begin embed video (more info about reffering embed videos: https://www.feedthebot.com/pagespeed/defer-videos.htm) -->
                                <div class="embed-responsive embed-responsive-16by9">
                                    <video src="<?php echo $v->link; ?>" width="500" height="281" poster="<?php echo $v->preview; ?>" controls />
                                    <!--iframe data-src="https://player.vimeo.com/video/60616160?" width="500" height="281" allowfullscreen></iframe-->
                                </div>
                                <!-- End embed video -->

                            </div> <!-- /.col-inner -->
                        </div> <!-- /.col -->

                        <div class="col-right col-lg-6 col-lg-height col-middle no-padding bg-light-gray">
                            <div class="col-inner">

                                <!-- Begin Heading -->
                                <div class="heading heading-xlg">
                                    <h1>Видео <span class="bg-main text-white">#<?php echo $v->number; ?></span></h1>
                                </div>
                                <!-- End Heading -->

                                <p class="lead"><?php echo $v->description; ?></p>
                            </div> <!-- /.col-inner -->
                        </div> <!-- /.col -->

                    </div>
                    <!-- End responsive columns of same height -->

                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section>
    <?php else : ?>
        <section id="section-3" class="video-demonstration split-box no-padding hidden-xs hidden-sm hidden-md visible-lg">
            <div class="container-fluid">
                <div class="row">                                <!-- Begin responsive columns of same height (more info: http://www.minimit.com/articles/solutions-tutorials/bootstrap-3-responsive-columns-of-same-height) -->
                    <div class="row-same-height">
                        <div class="col-left col-lg-6 col-lg-height col-middle no-padding bg-light-gray">
                            <div class="col-inner">
                                <!-- Begin Heading -->
                                <div class="heading heading-xlg">
                                    <h1>Видео <span class="bg-main text-white">#<?php echo $v->number; ?></span></h1>
                                </div>
                                <!-- End Heading -->

                                <p class="lead"><?php echo $v->description; ?></p>
                            </div> <!-- /.col-inner -->
                        </div> <!-- /.col -->

                        <div class="col-right col-lg-6 col-lg-height no-padding">
                            <div class="col-inner no-padding">

                                <!-- Begin embed video (more info about reffering embed videos: https://www.feedthebot.com/pagespeed/defer-videos.htm) -->
                                <div class="embed-responsive embed-responsive-16by9">
                                    <!--iframe data-src="https://player.vimeo.com/video/60616160?" width="500" height="281" allowfullscreen></iframe-->
                                    <video src="<?php echo $v->link; ?>" width="500" height="281" poster="<?php echo $v->preview; ?>" controls />
                                </div>
                                <!-- End embed video -->

                            </div> <!-- /.col-inner -->
                        </div> <!-- /.col -->


                    </div>
                    <!-- End responsive columns of same height -->

                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section>

        <!-- Section 3.5 -->
        <section id="section-3_5" class="video-demonstration split-box no-padding visible-xs visible-sm visible-md hidden-lg">
            <div class="container-fluid">
                <div class="row">
                    <!-- Begin responsive columns of same height (more info: http://www.minimit.com/articles/solutions-tutorials/bootstrap-3-responsive-columns-of-same-height) -->
                    <div class="row-same-height">

                        <div class="col-right col-lg-6 col-lg-height no-padding">
                            <div class="col-inner no-padding">

                                <!-- Begin embed video (more info about reffering embed videos: https://www.feedthebot.com/pagespeed/defer-videos.htm) -->
                                <div class="embed-responsive embed-responsive-16by9">
                                    <!--iframe data-src="https://player.vimeo.com/video/60616160?" width="500" height="281" allowfullscreen></iframe-->
                                    <video src="<?php echo $v->link; ?>" width="500" height="281" poster="<?php echo $v->preview; ?>" controls />
                                </div>
                                <!-- End embed video -->

                            </div> <!-- /.col-inner -->
                        </div> <!-- /.col -->

                        <div class="col-left col-lg-6 col-lg-height col-middle no-padding bg-light-gray">
                            <div class="col-inner">

                                <!-- Begin Heading -->
                                <div class="heading heading-xlg">
                                    <h1>Видео <span class="bg-main text-white">#<?php echo $v->number; ?></span></h1>
                                </div>
                                <!-- End Heading -->

                                <p class="lead"><?php echo $v->description; ?></p>
                            </div> <!-- /.col-inner -->
                        </div> <!-- /.col -->

                    </div>
                    <!-- End responsive columns of same height -->

                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section>
    <?php
    endif;
    $last_number = $v->number;
    $odd = !$odd;
endforeach;
?>