<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php wp_title( '«', TRUE, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
  <meta name="description" content="Эстафета фестивалей межрайонных советов директоров школ">
  <meta name="keywords" content="Эстафета фестивалей межрайонных советов директоров школ, МРСД, МЦКО"/>
  <meta name="author" content="http://mcko.ru">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Google fonts - (more info: https://www.google.com/fonts) -->
	<link href="http://fonts.googleapis.com/css?family=Oswald:400,700,300" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- Libs and Plugins CSS -->
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/vendor/bootstrap/css/bootstrap.min.css">
	<!-- bootstrap CSS (more info: http://getbootstrap.com) -->
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/vendor/font-awesome/css/font-awesome.min.css">
	<!-- Font Icons (more info: http://fortawesome.github.io/Font-Awesome) -->
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/vendor/ytplayer/css/jquery.mb.YTPlayer.min.css">
	<!-- YTPlayer JS (more info: https://github.com/pupunzi/jquery.mb.YTPlayer) -->

	<!-- owl carousel JS (more info: http://www.owlcarousel.owlgraphic.com) -->
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/vendor/owl-carousel/css/owl.carousel.css">
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/vendor/owl-carousel/css/owl.carousel.plugins.css">
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/vendor/owl-carousel/css/owl.theme.default.css">

	<!-- Theme navigation menu CSS (more info: http://codyhouse.co/gem/secondary-expandable-navigation) -->
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/menu.css">

	<!-- Theme helper classes CSS -->
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/helper.css">

	<!-- Theme master CSS -->
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/theme.css">

	<!-- Theme custom CSS (all your CSS customizations) -->
	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/custom.css">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>
	<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit&hl=ru" async defer></script>
</head>

<body>
