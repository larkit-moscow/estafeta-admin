</div> <!-- /.body-content -->

<!-- /////////////////////////////////
////////// End body content //////////
////////////////////////////////// -->

<!-- список участников -->
<div id="admit-list">
	<div id="admit-list-inner">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>
						<span class="text-white">Список</span><br/>
						<span class="bg-main text-white">Участников</span>
					</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<ol>
						<li>05.09.2015 <a href="http://lyc1557zg.mskobr.ru/">Лицей №1557</a></li>
						<li>12.09.2015 <a href="http://sch1228.mskobr.ru/">Школа №1228 с углубленным изучением
								английского языка</a></li>
						<li>12.09.2015 <a href="http://sch1400z.mskobr.ru/">Школа №1400</a></li>
						<li>19.09.2015 <a href="http://sch1494sv.mskobr.ru/">Школа №1494</a></li>
						<li>19.09.2015 <a href="http://sch2065tn.mskobr.ru/">Школа №2065</a></li>
						<li>26.09.2015 <a href="http://gym1637.mskobr.ru/">Гимназия №1637</a></li>
						<li>26.09.2015 <a href="http://sch2098s.mskobr.ru/">Школа №2098 им. Героя Советского Союза Л.М.
								Доватора</a></li>
						<li>03.10.2015 <a href="http://sch1298sz.mskobr.ru/">Школа №1298</a></li>
						<li>03.10.2015 <a href="http://sch2017u.mskobr.ru/">Школа №2017</a></li>
						<li>17.10.2015 <a href="http://kurchat.mskobr.ru/">Курчатовская школа</a></li>
						<li>17.10.2015 <a href="http://gym1409s-new.mskobr.ru/">Гимназия №1409</a></li>
						<li>24.10.2015 <a href="http://sch1259.mskobr.ru/">Школа № 1259 с углубленным изучением
								иностранных языков</a></li>
						<li>24.10.2015 <a href="http://gym1554.mskobr.ru/">Гимназия № 1554</a></li>
						<li>31.10.2015 <a href="http://sch2036v.mskobr.ru/">Школа №2036</a></li>
						<li>31.10.2015 <a href="http://sch534.mskobr.ru/">Школа №534</a></li>
						<li>14.11.2015 <a href="http://sch1357uv.mskobr.ru/">Школа №1357 с углубленным изучением
								английского языка</a></li>
						<li>14.11.2015 <a href="http://sch1995uz.mskobr.ru/">Школа №1995</a></li>
						<li>21.11.2015 <a href="http://gum1573.mskobr.ru/">Гимназия №1573</a></li>
						<li>21.11.2015 <a href="http://sch1002.mskobr.ru/">Школа №1002</a></li>
					</ol>
				</div>
				<div class="col-md-5 col-md-offset-1">
					<ol start="20">
						<li>28.11.2015 <a href="http://sch2089uv.mskobr.ru/">Школа №2089</a></li>
						<li>28.11.2015 <a href="http://sch97z.mskobr.ru/">Школа №97</a></li>
						<li>05.12.2015 <a href="http://gym1508.mskobr.ru/">Измайловская гимназия №1508</a></li>
						<li>05.12.2015 <a href="http://sch924.mskobr.ru/">Школа №924</a></li>
						<li>12.12.2015 <a href="http://gym1532uz.mskobr.ru/">Школа №1352 с углубленным изучением
								английского языка</a></li>
						<li>12.12.2015 <a href="http://sch1474s.mskobr.ru/">Школа №1474</a></li>
						<li>16.01.2016 <a href="http://sch1394uv.mskobr.ru/">Школа №1394 с углубленным изучением
								отдельных предметов</a></li>
						<li>16.01.2016 <a href="http://sch1363uv.mskobr.ru/">Школа №1363 с углубленным изучением
								отдельных предметов</a></li>
						<li>23.01.2016 <a href="http://2123.mskobr.ru/">Школа №2123 им. Мигеля Эрнандеса</a></li>
						<li>23.01.2016 <a href="http://sch2009uz.mskobr.ru/">Школа №2009</a></li>
						<li>06.02.2016 <a href="http://gym1538sz.mskobr.ru/">Гимназия №1538</a></li>
						<li>06.02.2016 <a href="http://sch630.mskobr.ru/">Школа №630 «Лингвистический центр»</a></li>
						<li>13.02.2016 <a href="http://gym1599uv.mskobr.ru/">Лицейско-гимназический комплекс на
								Юго-Востоке</a></li>
						<li>13.02.2016 <a href="http://sch1206uz.mskobr.ru/">Школа №1206 с углубленным изучением
								английского языка</a></li>
						<li>19.03.2016 <a href="Школа № 2054">Школа №2054</a></li>
						<li>19.03.2016 <a href="http://gym1532uz.mskobr.ru/">Гимназия №1532</a></li>
						<li>16.04.2016 <a href="http://sch7uz.mskobr.ru/">Школа №7 с углубленным изучением математики и
								информатики</a></li>
						<li>16.04.2016 <a href="http://sch1329.mskobr.ru/">Школа № 1329</a></li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="#" class="btn btn-white btn-rounded btn-lg close-list cclose-list">Закрыть</a>
			</div>
		</div>

		<a href="#" class="close-list close1-list"></a>

	</div>
</div>

<!-- Scroll to top button -->
<a href="#" class="scrolltotop hidden-xs"><i class="fa fa-arrow-up"></i></a>

<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function(d, w, c){
		(w[c] = w[c] || []).push(function(){
			try {
				w.yaCounter32400430 = new Ya.Metrika({
					id: 32400430,
					clickmap: true,
					trackLinks: true,
					accurateTrackBounce: true,
					webvisor: true
				});
			} catch (e) {
			}
		});
		var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function(){
			n.parentNode.insertBefore(s, n);
		};
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";
		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else {
			f();
		}
	})(document, window, "yandex_metrika_callbacks");</script>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/32400430" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript><!-- /Yandex.Metrika counter -->

<!-- Libs and Plugins JS -->
<!-- script src="<?php bloginfo( 'template_url' ); ?>/vendor/jquery/jquery-1.11.1.min.js"></script --> <!-- jquery JS (more info: https://jquery.com) -->
<script src="<?php bloginfo( 'template_url' ); ?>/vendor/bootstrap/js/bootstrap.min.js"></script> <!-- bootstrap JS (more info: http://getbootstrap.com) -->
<script src="<?php bloginfo( 'template_url' ); ?>/vendor/jquery.easing.min.js"></script> <!-- jquery easing JS (more info: http://gsgd.co.uk/sandbox/jquery/easing) -->
<script src="<?php bloginfo( 'template_url' ); ?>/vendor/jquery.stellar.min.js"></script> <!-- parallax JS (more info: http://markdalgleish.com/projects/stellar.js) -->
<script src="<?php bloginfo( 'template_url' ); ?>/vendor/smoothscroll.js"></script> <!-- smoothscroll JS (more info: https://gist.github.com/theroyalstudent/4e6ec834be19bf077298/) -->
<script src="<?php bloginfo( 'template_url' ); ?>/vendor/jquery.counterup.min.js"></script> <!-- counter up JS (requires jQuery "waypoints.js" plugin. more info: https://github.com/bfintal/Counter-Up) -->
<script src="<?php bloginfo( 'template_url' ); ?>/vendor/waypoints.min.js"></script> <!-- counter up JS (more info: https://github.com/bfintal/Counter-Up) -->

<script src="<?php bloginfo( 'template_url' ); ?>/vendor/masonry.pkgd.min.js"></script> <!-- masonry JS (more info: http://masonry.desandro.com/) -->
<script src="<?php bloginfo( 'template_url' ); ?>/vendor/imagesloaded.pkgd.min.js"></script> <!-- imagesloaded JS (more info: http://masonry.desandro.com/appendix.html#imagesloaded) -->

<script src="<?php bloginfo( 'template_url' ); ?>/vendor/owl-carousel/js/owl.carousel.js"></script> <!-- owl carousel JS (more info: http://www.owlcarousel.owlgraphic.com) -->
<script src="<?php bloginfo( 'template_url' ); ?>/vendor/owl-carousel/js/owl.carousel.plugins.js"></script>

<script src="<?php bloginfo( 'template_url' ); ?>/vendor/ytplayer/js/jquery.mb.YTPlayer.min.js"></script> <!-- YTPlayer JS (more info: https://github.com/pupunzi/jquery.mb.YTPlayer) -->

<!-- Theme JS -->
<!-- script src="<?php bloginfo( 'template_url' ); ?>/js/theme.js"></script -->

<!-- Theme custom JS (all your JS customizations) -->
<!-- script src="<?php bloginfo( 'template_url' ); ?>/js/custom.js"></script -->

<!--==============================
///// End Google Analytics /////
============================== -->

<!--==============================
///// Begin Google Analytics /////
============================== -->

<!-- Paste your Google Analytics code here.
Go to http://www.google.com/analytics/ for more information. -->

<?php wp_footer(); ?>

</body>
</html>
