<?php
/* Using for server cron video update, without WP Cron */

include( '../../../wp-config.php' );

$mysqli = new mysqli( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );
if ( $mysqli->connect_errno ) {
	echo "Sorry, the connection is lost.";
	exit;
}

$vote_date = get_option( 'vote_date' );
$message = 'Server cron is activated' . "\n";
if ( $vote_date ) {
	$current_timestamp = time();
	$message .= 'Current timestamp (when function was activated): ' . date( 'H:i:s d.m.Y', $current_timestamp ) . ' (' . $current_timestamp . ')' . "\n";
	if ( $vote_date <= $current_timestamp ) {// если время настало (автоматом делаем + неделю и заменяем видео)
		// переносим текущие в архив
		$message .= 'Current vote date is less than current timestamp. Getting update of the video.' . "\n";
		$message .= 'Current vote date: ' . date( 'H:i:s d.m.Y', $vote_date ) . ' (' . $vote_date . ')' . "\n";

		$sql = "UPDATE " . $table_prefix . "larvid SET status='-1', archieved='" . $vote_date . "' WHERE status='1'";
		$mysqli->query( $sql );

		// переносим отложенные в текущие
		$sql = "UPDATE " . $table_prefix . "larvid SET status='1' WHERE status='0'";
		$mysqli->query( $sql );

		// увеличиваем время голосования на неделю
		$new_vote_date = $vote_date + ( 7 * 24 * 60 * 60 );
		update_option( 'vote_date', $new_vote_date );
		$message .= 'The video was updated. Setting the new vote date.' . "\n";
		$message .= 'New vote date: ' . date( 'H:i:s d.m.Y', $new_vote_date ) . ' (' . $new_vote_date . ')' . "\n";
	}
}
$filename = 'larvid_server_cron.log';
$fpe = fopen( $filename, 'a' );
fwrite( $fpe, $message );
fclose( $fpe );
?>
