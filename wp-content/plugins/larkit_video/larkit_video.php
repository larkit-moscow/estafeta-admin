<?php
/*
Plugin Name: Vote for Video
Description: Upload video and set vote count
Version: 1.0
Author: Leonid Vorozheikin
Author URI: bazoon@bk.ru
*/

define( 'LOG_MODE', TRUE );
define( 'WP_CRON_DISABLED', TRUE );

define( 'LARVID_DIR', plugin_dir_path( __FILE__ ) );
define( 'LARVID_URL', plugin_dir_url( __FILE__ ) );
define( 'LARVID_TABLE_NAME', 'wp_larvid' );

register_activation_hook( __FILE__, 'larvid_activation' );
register_deactivation_hook( __FILE__, 'larvid_deactivation' );

add_action( 'admin_menu', 'larvid_menu' );
add_action( 'admin_head', 'larvid_register_head' );
if ( WP_CRON_DISABLED !== FALSE ) {
	add_action( 'cron_hourly', 'larvid_cron' );
} else {
	remove_action( 'cron_hourly', 'larvid_cron' );
}
//add_action('generate_rewrite_rules', 'larvid_add_rewrite_rules');
//add_filter( 'query_vars','larvid_insert_query_vars' );

// роутинг
/*function larvid_add_rewrite_rules($wp_rewrite) {
    $rules = array(
        'vote/(.*?)$'  => 'index.php?video_id=$matches[1]',
        'unvote/(.*?)$'  => 'index.php?video_id=$matches[1]',
    );
    $wp_rewrite->rules = $rules + (array)$wp_rewrite->rules;
}

function larvid_insert_query_vars($vars) {
    array_push($vars, 'video_id');
    return $vars;
}*/

// активация плагина
function larvid_activation() {
	global $wpdb;
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$sql = "CREATE TABLE IF NOT EXISTS `wp_larvid` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `number` int(11) NOT NULL,
      `description` text NOT NULL,
      `link` varchar(255) NOT NULL,
      `votes` int(11) NOT NULL DEFAULT '0',
      `rate` int(11) NOT NULL DEFAULT '0',
      `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 - отложен, 1 - голосование, -1 - архив',
      `preview` varchar(255) NOT NULL DEFAULT '',
      `archieved` int(11) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

	// создаем таблицу
	dbDelta( $sql );

	//add_action('generate_rewrite_rules', 'larvid_add_rewrite_rules');
	//global $wp_rewrite;
	//$wp_rewrite->flush_rules(); // force call to generate_rewrite_rules()

	wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'cron_hourly' );
}

function larvid_deactivation() {
	wp_clear_scheduled_hook( 'cron_hourly' );
}

// приклеиваем css
function larvid_register_head() {
	$url = LARVID_URL . '/css/larkit_video.css';
	echo "<link rel='stylesheet' type='text/css' href='$url' />\n";
}

// cron
function larvid_cron() {
	if ( WP_CRON_DISABLED !== FALSE ) {
		$vote_date = get_option( 'vote_date' );
		$message = 'Fuction larvid_cron activated' . "\n";
		if ( $vote_date ) {
			$current_timestamp = time();
			$message .= 'Current timestamp (when function was activated): ' . date( 'H:i:s d.m.Y', $current_timestamp ) . ' (' . $current_timestamp . ')' . "\n";
			if ( $vote_date <= $current_timestamp ) {// если время настало (автоматом делаем + неделю и заменяем видео)
				global $wpdb;
				// переносим текущие в архив
				$message .= 'Current vote date is less than current timestamp. Getting update of the video.' . "\n";
				$message .= 'Current vote date: ' . date( 'H:i:s d.m.Y', $vote_date ) . ' (' . $vote_date . ')' . "\n";

				$wpdb->update( LARVID_TABLE_NAME, array(
					'status' => - 1,
					'archieved' => $vote_date,
				), array( 'status' => 1 ), array( '%d' ) );
				// переносим отложенные в текущие
				$wpdb->update( LARVID_TABLE_NAME, array(
					'status' => 1,
				), array( 'status' => 0 ), array( '%d' ) );

				// увеличиваем время голосования на неделю
				$new_vote_date = $vote_date + ( 7 * 24 * 60 * 60 );
				update_option( 'vote_date', $new_vote_date );
				$message .= 'The video was updated. Setting the new vote date.' . "\n";
				$message .= 'New vote date: ' . date( 'H:i:s d.m.Y', $new_vote_date ) . ' (' . $new_vote_date . ')' . "\n";
			}
		}
		$message .= '-------------------------------' . "\n";
		if ( LOG_MODE === TRUE ) {
			larvit_wp_cron_log( LARVID_DIR . 'logs/larvid_wp_cron.log', $message );
		}
	}
}

// ====================================================== АДМИНКА ======================================================
function larvid_menu() {
	//    add_options_page( 'Vote for Video', 'Vote for Video', 'manage_options', 'larvid', 'larvid_options' );
	add_menu_page( 'Vote for Video', 'Vote for Video', 'manage_options', 'larvid', 'larvid_options' );
}

function larvid_options() {
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	// если нажато добавить
	if ( ! empty( $_POST['add_video'] ) ) {
		larvid_add_video( $_POST['number'], $_POST['link'], $_POST['description'], $_POST['votes'], $_POST['rate'], $_POST['status'], $_POST['preview'] );
	} // если нажато сохранить
	else if ( ! empty( $_POST['edit_video'] ) ) {
		larvid_update_video( $_POST['video_id'], $_POST['number'], $_POST['link'], $_POST['description'], $_POST['votes'], $_POST['rate'], $_POST['status'], $_POST['preview'] );
	} // если нажато удалить
	else if ( ! empty( $_POST['del_video'] ) ) {
		larvid_delete_video( $_POST['video_id'] );
	}

	echo '<table id="larkit-video" class="wp-list-table widefat fixed striped"><thead>
        <tr>
            <th>№</th>
            <th>Голосов</th>
            <th>Рейтинг</th>
            <th>Ссылка</th>
            <th>Предпросмотр</th>
    		<th>Статус</th>
    		<th>Описание</th>
    		<th>&nbsp;</th>
        </tr></thead><tbody>';

	// получаем список видео
	echo '<div class="wrap"><h2>Видео для голосования</h2>';
	$videos = larvid_get_videos();
	if ( ! empty( $videos ) ) {
		foreach ( $videos as $v ) {
			echo '<tr>
                    <form action="" method="post">
                        <input type="hidden" name="video_id" value="' . $v->id . '" />
                        <td><input type="text" name="number" value="' . $v->number . '" /></td>
                        <td><input type="text" name="votes" value="' . $v->votes . '" /></td>
                        <td><input type="text" name="rate" value="' . $v->rate . '" /></td>
                        <td><input type="text" name="link" value="' . $v->link . '" /></td>
						<td><input type="text" name="preview" value="' . $v->preview . '" /></td>
						<td>
							<select name="status">
								<option value="0"' . ( ( $v->status == 0 ) ? ' selected="selected"' : '' ) . '>Отложен</option>
								<option value="1"' . ( ( $v->status == 1 ) ? ' selected="selected"' : '' ) . '>На голосовании</option>
								<option value="-1"' . ( ( $v->status == - 1 ) ? ' selected="selected"' : '' ) . '>Архив</option>
							</select>
						</td>
                        <td><textarea name="description" cols="3">' . $v->description . '</textarea></td>
                        <td><input type="submit" name="edit_video" value="Сохранить" class="button action" /><input type="submit" name="del_video" value="Удалить" class="button action" /></td>
                    </form>
                  </tr>';
		}
	} else {
		echo '<tr><td colspan="7" class="larvid-not-found">Ни одного видео не добавлено!</td></tr>';
	}

	echo '<tr>
            <form action="" method="post">
                <td><input type="text" name="number" value="" /></td>
                <td><input type="text" name="votes" value="" /></td>
                <td><input type="text" name="rate" value="" /></td>
                <td><input type="text" name="link" value="" /></td>
    			<td><input type="text" name="preview" value="" /></td>
				<td>
					<select name="status">
						<option value="0">Отложен</option>
						<option value="1">На голосовании</option>
						<option value="-1">Архив</option>
					</select>
				</td>
                <td><textarea name="description" cols="3"></textarea></td>
                <td><input type="submit" name="add_video" value="Добавить" class="button action" /></td>
            </form>
          </tr>';

	echo '</tbody></table>';

	$vote_date = get_option( 'vote_date' );
	if ( ! $vote_date ) { // создаем параметр, если его нет
		update_option( 'vote_date', time() + 7 * 24 * 60 * 60 );
	}

	// сохраняем значение, если заполнена форма
	if ( ! empty( $_POST['save_vote_time'] ) ) {
		$vote_date = strtotime( $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'] . ' ' . $_POST['hour'] . ':00:00' );
		if ( $vote_date ) {
			update_option( 'vote_date', $vote_date );
		}
	}

	// окончание времени голосования
	echo '<h3>Окончание текущего голосования</h3>
    		<form action="" method="post">
                День: <input type="text" name="day" size="3" value="' . date( 'd', $vote_date ) . '" />
                Месяц: <input type="text" name="month" size="3" value="' . date( 'm', $vote_date ) . '" />
                Год: <input type="text" name="year" size="3" value="' . date( 'Y', $vote_date ) . '" />
                Час: <input type="text" name="hour" size="3" value="' . date( 'H', $vote_date ) . '" />
				<input type="submit" name="save_vote_time" value="Сохранить" class="button action" />
            </form>';

	echo '</div>';

	// reschedule hourly event larvid_cron() for WP-Cron if event is not exist in WP-Cron schedule
	if ( WP_CRON_DISABLED !== TRUE ) {
		$schedule = wp_get_schedule( 'cron_hourly' );
		if ( $schedule != 'hourly' ) {
			wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'cron_hourly' );
		}
	}
}

// ====================================================== МОДЕЛЬ =======================================================
function larvid_get_videos() {
	global $wpdb;
	$sql = "SELECT * FROM `" . LARVID_TABLE_NAME . "` ORDER BY `id` DESC, `status`, `number` DESC";
	$result = $wpdb->get_results( $sql, OBJECT );

	return $result;
}

// получаем активные видео
function larvid_get_active_videos() {
	global $wpdb;
	$sql = "SELECT * FROM `" . LARVID_TABLE_NAME . "` WHERE `status` = 1 ORDER BY `number`";
	$result = $wpdb->get_results( $sql, OBJECT );

	return $result;
}

// получаем архивные видео
function larvid_get_archieved_videos( $from = 0 ) {
	global $wpdb;
	$sql = "SELECT * FROM `" . LARVID_TABLE_NAME . "` WHERE `status` = -1 AND `archieved` != 0";
	if ( $from ) {
		$sql .= " AND `number` > " . intval( $from );
	}
	$sql .= " ORDER BY `number`";
	$result = $wpdb->get_results( $sql, OBJECT );

	$weeks = array();
	foreach ( $result as $v ) {
		$weeks[ $v->archieved ][] = $v;
	}

	return $weeks;
}

// получаем отложенные видео
function larvid_get_new_videos() {
	global $wpdb;
	$sql = "SELECT * FROM `" . LARVID_TABLE_NAME . "` WHERE `status` = 0 ORDER BY `number`";
	$result = $wpdb->get_results( $sql, OBJECT );

	return $result;
}

function larvid_get_video( $id ) {
	global $wpdb;
	$sql = "SELECT * FROM `" . LARVID_TABLE_NAME . "` WHERE `id` = " . intval( $id );
	$result = $wpdb->get_row( $sql, OBJECT );

	return $result;
}

function larvid_add_video( $number, $link, $description = '', $votes = 0, $rate = 0, $status = 0, $preview = '' ) {
	global $wpdb;
	$result = $wpdb->insert( LARVID_TABLE_NAME, array(
		'number' => $number,
		'description' => $description,
		'link' => $link,
		'votes' => $votes,
		'rate' => $rate,
		'status' => $status,
		'preview' => $preview,
	), array( '%d', '%s', '%s', '%d', '%d', '%d', '%s' ) );

	return $result;
}

function larvid_update_video( $id, $number, $link, $description = '', $votes = 0, $rate = 0, $status = 0, $preview = '' ) {
	global $wpdb;
	$result = $wpdb->update( LARVID_TABLE_NAME, array(
		'number' => $number,
		'description' => $description,
		'link' => $link,
		'votes' => $votes,
		'rate' => $rate,
		'status' => $status,
		'preview' => $preview,
	), array( 'id' => $id ), array( '%d', '%s', '%s', '%d', '%d', '%s', '%s' ) );

	return $result;
}

function larvid_delete_video( $id ) {
	global $wpdb;
	$sql = "DELETE FROM `" . LARVID_TABLE_NAME . "` WHERE `id` = " . intval( $id );
	$result = $wpdb->query( $sql );
	$wpdb->print_error();

	return $result;
}

// голосуем
function larvid_like( $id ) {
	$v = larvid_get_video( $id );
	if ( $v->status == 1 ) { // если видео на голосовании
		larvid_update_video( $id, $v->number, $v->link, $v->description, ( $v->votes + 1 ), ( $v->rate + 1 ), $v->status, $v->preview );
		$v->votes ++;
	}

	return $v->votes;
}

function larvid_unlike( $id ) {
	$v = larvid_get_video( $id );
	if ( $v->status == 1 ) { // если видео на голосовании
		larvid_update_video( $id, $v->number, $v->link, $v->description, ( $v->votes + 1 ), ( $v->rate - 1 ), $v->status, $v->preview );
		$v->votes ++;
	}

	return $v->votes;
}

// записываем сообщение в файл, используется для логирования WP Cron
function larvit_wp_cron_log( $filename, $message ) {
	$fpe = fopen( $filename, 'a' );
	fwrite( $fpe, $message );
	fclose( $fpe );
}
